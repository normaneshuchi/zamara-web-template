jQuery(document).ready(function(){

    var btn = $('#backToTopButton');
    var dlBtn = $('#downloadButton')

    btn.on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({scrollTop:0}, '300');
      });
    dlBtn.on('click', function(e){
        e.preventDefault();
        window.open('./assets/doc/temp.pdf')
    })
});